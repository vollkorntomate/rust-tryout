use std::io::{self, Write};

fn collatz(n: u64) -> u64 {
    if n <= 1 {
        return n;
    }
    println!("{}", n);

    if n % 2 == 0 {
        return collatz(n / 2);
    } else {
        return collatz(3 * n + 1);
    }
}

pub fn main() {
    print!("Input a number: ");
    io::stdout().flush().expect("Error!");

    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("Failed to read line");

    let n: u64 = input.trim().parse().expect("Input not an u64");

    let res = collatz(n);
    println!("{}", res);
}