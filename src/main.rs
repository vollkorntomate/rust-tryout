use std::io::{self, Write};

mod collatz;

fn main() {
    print!("Choose an example: ");
    io::stdout().flush().expect("Error flushing stdout");

    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Error reading from stdin");

    match input.trim() {
        "collatz" => collatz::main(),
        _ => {
            println!("Unknown example! Try again...");
            main()
        },
    }
}